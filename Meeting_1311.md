# EBS CS infrastructure meeting held the 13/11

Present: ACU team - Didier Gervaise - Elodie Benoit - Benoit Rousselle 

## Hardware


- VME: We will push to get rid of them. 
    - FE will look into replacement for VideoMux and VideoMatrix
    - JMC will look into VPDU still in use today (with help of George Gautier)

- We do not support Field Bus anymore and therefore the VRIF boards

- The host **vmesrv1** and **vmesrv2** will be shutdown. The software running on them
(bootp - tftp) will be moved to another hosts (or VM)

- We will push to update all our crates to Debian 9. The same is true for our servers
(except fdasrv which will stay with Debian 7 for hardware reasons). The VM's will also run 
Debian 9

- The two hypervisors we already have (kvmmcs1 and kvmmcs2) are good enought for the EBS and
should be powerfull enought to run all the VM we will have (around 30 VM)

- **Orion** and its spare will be shutdown (too old). The EBS Tango database will be installed
on **hdbsrv** (which also has a spare for the Db replication). Those two hosts will be available
only from 04/2019. In the meantime, the new EBS Tango database and its spare will run on VM

- Taco will be stopped. If we still need device server for VPDU, VDSP71 or VideoMux
boards, we will re-write them as Tango DS

- **Neptune** will be shutdown

- **Vega** (CTRM DNS server) will be shutdown

- **sirius2** will be used to run orion database in read only mode

- We will run more than 1 DS on alarmsrv!

- Tango for Red Hat 4 is frozen. There is a Red Hat 4 host for development but it is not
maintainable any more for TID. We need to push to get rid of Red Hat $ (VME host and the VM
running it)

## Databases

- Tango database will be MariaDb. Someone has to look into how to set-up MariaDb in order
to have replication on a second host. 
JMC will try to find out if icepapcms support MariaDB as DB (Manu Perez does not know)

- The Tango database orion will be moved to a smaller computer and will be kept as read-only
For EBS db, we will minimize the number of device domain names. INjector system devices
will move from one DB to another with sometimes some renaining.

- The former HDB will be stopped and only the long term archiving will stay

- **ltasrv** will stay as it is today

## Storage


- The /operation FS will be moved to NetApp and ctrmfiler will be shutdown. The /operation/dserver
folder and the operator account home will be moved to other place and will be switch to read-only
files. For EBS, we will start with an empty operator account and with an empty /operation/dserver
folder




